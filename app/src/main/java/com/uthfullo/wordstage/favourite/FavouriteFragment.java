package com.uthfullo.wordstage.favourite;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.uthfullo.wordstage.R;
import com.uthfullo.wordstage.words.WordActivity;
import com.uthfullo.wordstage.words.db.vocabs;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FavouriteFragment extends Fragment{

    @Bind(R.id.my_recycler_view) RecyclerView recyclerView;
    @Bind(R.id.emptyImg) ImageView emptyImage;
    FavouriteViewModel viewModel;
    FavouriteAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_favourite, null);
        ButterKnife.bind(this, view);

        adapter = new FavouriteAdapter(new ArrayList<vocabs>());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(getActivity()).get(FavouriteViewModel.class);
        viewModel.getFavouriteItemList().observe(this, new Observer<List<vocabs>>() {
            @Override
            public void onChanged(@Nullable List<vocabs> vocabs) {

                if(vocabs.size() > 0) {
                    emptyImage.setVisibility(View.GONE);
                    adapter.addItems(vocabs);
                }else{
                    adapter.clearItems();
                    emptyImage.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }

}
